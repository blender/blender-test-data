==== Meshes: 1
- Mesh 'Mesh' vtx:24 face:22 loop:88 edge:44
    - 21 22 3 2 2 ... 22 20 23 19 16 
    - 0/1 2/3 4/5 6/7 0/9 ... 3/22 21/22 19/23 22/23 20/23 
  - attr 'position' FLOAT_VECTOR POINT
    - (-0.500, 0.015, 0.500)
    - (0.500, 0.015, 0.500)
    - (-0.500, 2.716, 0.500)
      ...
    - (-0.500, 2.176, 0.500)
    - (0.500, 2.176, 0.500)
    - (0.500, 2.176, -0.500)
  - attr 'sharp_edge' BOOLEAN EDGE
    - 1 1 1 1 1 ... 1 0 1 0 0 
  - attr 'material_index' INT FACE
    - 0 0 0 0 0 ... 0 0 0 0 0 
  - attr 'custom_normal' INT16_2D CORNER
    - (0, 0)
    - (0, 0)
    - (0, 0)
      ...
    - (0, 0)
    - (0, 0)
    - (0, 0)
  - attr 'map1' FLOAT2 CORNER
    - (0.375, 0.200)
    - (0.625, 0.200)
    - (0.625, 0.250)
      ...
    - (0.625, 0.550)
    - (0.625, 0.600)
    - (0.375, 0.600)
  - vertex groups:
    - 0=1.000
    - 0=1.000
    - 2=1.000
    - 2=1.000
    - 2=1.000
  - 1 materials
    - 'lambert1' 

==== Objects: 2
- Obj 'Armature' ARMATURE data:'Armature'
  - pos 0.000, 0.000, 0.000
  - rot 1.571, 0.000, 0.000 (XYZ)
  - scl 0.010, 0.010, 0.010
  - anim act:Armature|wiggle|BaseLayer slot:OBArmature|wiggle|BaseLayer blend:REPLACE drivers:0
- Obj 'pCube1' MESH data:'Mesh' par:'Armature'
  - pos 0.000, 0.000, 0.000
  - rot 0.000, 0.000, 0.000 (XYZ)
  - scl 1.000, 1.233, 1.000
  - 3 vertex groups
    - 'joint1' 'joint2' 'joint3' 
  - 1 modifiers
    - ARMATURE 'Armature'
  - props: str:currentUVSet='map1'

==== Materials: 1
- Mat 'lambert1'
  - base color (0.500, 0.500, 0.500)
  - specular ior 0.500
  - specular tint (1.000, 1.000, 1.000)
  - roughness 0.553
  - metallic 0.000
  - ior 1.500
  - viewport diffuse (0.500, 0.500, 0.500, 1.000)
  - viewport specular (1.000, 1.000, 1.000), intensity 0.500
  - viewport metallic 0.000, roughness 0.553
  - backface False probe True shadow False

==== Actions: 3
- Action 'Armature|deform|BaseLayer' curverange:(41.0 .. 61.0) curves:30
  - fcu 'pose.bones["joint1"].location[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (41.0, 0.0) lh:(40.7, 0.0 AUTO_CLAMPED) rh:(41.3, 0.0 AUTO_CLAMPED)
    - (42.0, 0.0) lh:(41.7, 0.0 AUTO_CLAMPED) rh:(42.3, 0.0 AUTO_CLAMPED)
    - (43.0, 0.0) lh:(42.7, 0.0 AUTO_CLAMPED) rh:(43.3, 0.0 AUTO_CLAMPED)
      ...
    - (59.0, 0.0) lh:(58.7, 0.0 AUTO_CLAMPED) rh:(59.3, 0.0 AUTO_CLAMPED)
    - (60.0, 0.0) lh:(59.7, 0.0 AUTO_CLAMPED) rh:(60.3, 0.0 AUTO_CLAMPED)
    - (61.0, 0.0) lh:(60.7, 0.0 AUTO_CLAMPED) rh:(61.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].location[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (41.0, 0.0) lh:(40.7, 0.0 AUTO_CLAMPED) rh:(41.3, 0.0 AUTO_CLAMPED)
    - (42.0, 0.0) lh:(41.7, 0.0 AUTO_CLAMPED) rh:(42.3, 0.0 AUTO_CLAMPED)
    - (43.0, 0.0) lh:(42.7, 0.0 AUTO_CLAMPED) rh:(43.3, 0.0 AUTO_CLAMPED)
      ...
    - (59.0, 0.0) lh:(58.7, 0.0 AUTO_CLAMPED) rh:(59.3, 0.0 AUTO_CLAMPED)
    - (60.0, 0.0) lh:(59.7, 0.0 AUTO_CLAMPED) rh:(60.3, 0.0 AUTO_CLAMPED)
    - (61.0, 0.0) lh:(60.7, 0.0 AUTO_CLAMPED) rh:(61.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].location[2]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (41.0, 0.0) lh:(40.7, 0.0 AUTO_CLAMPED) rh:(41.3, 0.0 AUTO_CLAMPED)
    - (42.0, 0.0) lh:(41.7, 0.0 AUTO_CLAMPED) rh:(42.3, 0.0 AUTO_CLAMPED)
    - (43.0, 0.0) lh:(42.7, 0.0 AUTO_CLAMPED) rh:(43.3, 0.0 AUTO_CLAMPED)
      ...
    - (59.0, 0.0) lh:(58.7, 0.0 AUTO_CLAMPED) rh:(59.3, 0.0 AUTO_CLAMPED)
    - (60.0, 0.0) lh:(59.7, 0.0 AUTO_CLAMPED) rh:(60.3, 0.0 AUTO_CLAMPED)
    - (61.0, 0.0) lh:(60.7, 0.0 AUTO_CLAMPED) rh:(61.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].rotation_quaternion[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (41.0, 0.5) lh:(40.7, 0.5 AUTO_CLAMPED) rh:(41.3, 0.5 AUTO_CLAMPED)
    - (42.0, 0.9) lh:(41.7, 0.9 AUTO_CLAMPED) rh:(42.3, 1.0 AUTO_CLAMPED)
    - (43.0, 1.0) lh:(42.7, 1.0 AUTO_CLAMPED) rh:(43.3, 1.0 AUTO_CLAMPED)
      ...
    - (59.0, 1.0) lh:(58.7, 1.0 AUTO_CLAMPED) rh:(59.3, 1.0 AUTO_CLAMPED)
    - (60.0, 1.0) lh:(59.7, 1.0 AUTO_CLAMPED) rh:(60.3, 1.0 AUTO_CLAMPED)
    - (61.0, 1.0) lh:(60.7, 1.0 AUTO_CLAMPED) rh:(61.3, 1.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].rotation_quaternion[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (41.0, 0.9) lh:(40.7, 0.9 AUTO_CLAMPED) rh:(41.3, 0.9 AUTO_CLAMPED)
    - (42.0, 0.4) lh:(41.7, 0.6 AUTO_CLAMPED) rh:(42.3, 0.1 AUTO_CLAMPED)
    - (43.0, 0.0) lh:(42.7, 0.0 AUTO_CLAMPED) rh:(43.3, 0.0 AUTO_CLAMPED)
      ...
    - (59.0, 0.0) lh:(58.7, 0.0 AUTO_CLAMPED) rh:(59.3, 0.0 AUTO_CLAMPED)
    - (60.0, 0.0) lh:(59.7, 0.0 AUTO_CLAMPED) rh:(60.3, 0.0 AUTO_CLAMPED)
    - (61.0, 0.0) lh:(60.7, 0.0 AUTO_CLAMPED) rh:(61.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].rotation_quaternion[2]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (41.0, 0.0) lh:(40.7, 0.0 AUTO_CLAMPED) rh:(41.3, 0.0 AUTO_CLAMPED)
    - (42.0, 0.0) lh:(41.7, 0.0 AUTO_CLAMPED) rh:(42.3, 0.0 AUTO_CLAMPED)
    - (43.0, 0.0) lh:(42.7, 0.0 AUTO_CLAMPED) rh:(43.3, 0.0 AUTO_CLAMPED)
      ...
    - (59.0, 0.0) lh:(58.7, 0.0 AUTO_CLAMPED) rh:(59.3, 0.0 AUTO_CLAMPED)
    - (60.0, 0.0) lh:(59.7, 0.0 AUTO_CLAMPED) rh:(60.3, 0.0 AUTO_CLAMPED)
    - (61.0, 0.0) lh:(60.7, 0.0 AUTO_CLAMPED) rh:(61.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].rotation_quaternion[3]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (41.0, 0.0) lh:(40.7, 0.0 AUTO_CLAMPED) rh:(41.3, 0.0 AUTO_CLAMPED)
    - (42.0, 0.0) lh:(41.7, 0.0 AUTO_CLAMPED) rh:(42.3, 0.0 AUTO_CLAMPED)
    - (43.0, 0.0) lh:(42.7, 0.0 AUTO_CLAMPED) rh:(43.3, 0.0 AUTO_CLAMPED)
      ...
    - (59.0, 0.0) lh:(58.7, 0.0 AUTO_CLAMPED) rh:(59.3, 0.0 AUTO_CLAMPED)
    - (60.0, 0.0) lh:(59.7, 0.0 AUTO_CLAMPED) rh:(60.3, 0.0 AUTO_CLAMPED)
    - (61.0, 0.0) lh:(60.7, 0.0 AUTO_CLAMPED) rh:(61.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].scale[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (41.0, 1.0) lh:(40.7, 1.0 AUTO_CLAMPED) rh:(41.3, 1.0 AUTO_CLAMPED)
    - (42.0, 1.0) lh:(41.7, 1.0 AUTO_CLAMPED) rh:(42.3, 1.0 AUTO_CLAMPED)
    - (43.0, 1.0) lh:(42.7, 1.0 AUTO_CLAMPED) rh:(43.3, 1.0 AUTO_CLAMPED)
      ...
    - (59.0, 0.5) lh:(58.7, 0.5 AUTO_CLAMPED) rh:(59.3, 0.5 AUTO_CLAMPED)
    - (60.0, 0.5) lh:(59.7, 0.5 AUTO_CLAMPED) rh:(60.3, 0.5 AUTO_CLAMPED)
    - (61.0, 0.5) lh:(60.7, 0.5 AUTO_CLAMPED) rh:(61.3, 0.5 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].scale[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (41.0, 1.0) lh:(40.7, 1.0 AUTO_CLAMPED) rh:(41.3, 1.0 AUTO_CLAMPED)
    - (42.0, 1.0) lh:(41.7, 1.0 AUTO_CLAMPED) rh:(42.3, 1.0 AUTO_CLAMPED)
    - (43.0, 1.0) lh:(42.7, 1.0 AUTO_CLAMPED) rh:(43.3, 1.0 AUTO_CLAMPED)
      ...
    - (59.0, 1.0) lh:(58.7, 1.0 AUTO_CLAMPED) rh:(59.3, 1.0 AUTO_CLAMPED)
    - (60.0, 1.0) lh:(59.7, 1.0 AUTO_CLAMPED) rh:(60.3, 1.0 AUTO_CLAMPED)
    - (61.0, 1.0) lh:(60.7, 1.0 AUTO_CLAMPED) rh:(61.3, 1.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].scale[2]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (41.0, 1.0) lh:(40.7, 1.0 AUTO_CLAMPED) rh:(41.3, 1.0 AUTO_CLAMPED)
    - (42.0, 1.0) lh:(41.7, 1.0 AUTO_CLAMPED) rh:(42.3, 1.0 AUTO_CLAMPED)
    - (43.0, 1.0) lh:(42.7, 1.0 AUTO_CLAMPED) rh:(43.3, 1.0 AUTO_CLAMPED)
      ...
    - (59.0, 1.0) lh:(58.7, 1.0 AUTO_CLAMPED) rh:(59.3, 1.0 AUTO_CLAMPED)
    - (60.0, 1.0) lh:(59.7, 1.0 AUTO_CLAMPED) rh:(60.3, 1.0 AUTO_CLAMPED)
    - (61.0, 1.0) lh:(60.7, 1.0 AUTO_CLAMPED) rh:(61.3, 1.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint2"].location[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (41.0, 0.0) lh:(40.7, 0.0 AUTO_CLAMPED) rh:(41.3, 0.0 AUTO_CLAMPED)
    - (42.0, 0.0) lh:(41.7, 0.0 AUTO_CLAMPED) rh:(42.3, 0.0 AUTO_CLAMPED)
    - (43.0, 0.0) lh:(42.7, 0.0 AUTO_CLAMPED) rh:(43.3, 0.0 AUTO_CLAMPED)
      ...
    - (59.0, 0.0) lh:(58.7, 0.0 AUTO_CLAMPED) rh:(59.3, 0.0 AUTO_CLAMPED)
    - (60.0, 0.0) lh:(59.7, 0.0 AUTO_CLAMPED) rh:(60.3, 0.0 AUTO_CLAMPED)
    - (61.0, 0.0) lh:(60.7, 0.0 AUTO_CLAMPED) rh:(61.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint2"].location[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (41.0, -0.0) lh:(40.7, -0.0 AUTO_CLAMPED) rh:(41.3, -0.0 AUTO_CLAMPED)
    - (42.0, -0.0) lh:(41.7, -0.0 AUTO_CLAMPED) rh:(42.3, -0.0 AUTO_CLAMPED)
    - (43.0, -0.0) lh:(42.7, -0.0 AUTO_CLAMPED) rh:(43.3, -0.0 AUTO_CLAMPED)
      ...
    - (59.0, 0.7) lh:(58.7, 0.7 AUTO_CLAMPED) rh:(59.3, 0.7 AUTO_CLAMPED)
    - (60.0, 0.7) lh:(59.7, 0.7 AUTO_CLAMPED) rh:(60.3, 0.7 AUTO_CLAMPED)
    - (61.0, 0.7) lh:(60.7, 0.7 AUTO_CLAMPED) rh:(61.3, 0.7 AUTO_CLAMPED)
  - fcu 'pose.bones["joint2"].location[2]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (41.0, 0.0) lh:(40.7, 0.0 AUTO_CLAMPED) rh:(41.3, 0.0 AUTO_CLAMPED)
    - (42.0, 0.0) lh:(41.7, 0.0 AUTO_CLAMPED) rh:(42.3, 0.0 AUTO_CLAMPED)
    - (43.0, 0.0) lh:(42.7, 0.0 AUTO_CLAMPED) rh:(43.3, 0.0 AUTO_CLAMPED)
      ...
    - (59.0, 0.0) lh:(58.7, 0.0 AUTO_CLAMPED) rh:(59.3, 0.0 AUTO_CLAMPED)
    - (60.0, 0.0) lh:(59.7, 0.0 AUTO_CLAMPED) rh:(60.3, 0.0 AUTO_CLAMPED)
    - (61.0, 0.0) lh:(60.7, 0.0 AUTO_CLAMPED) rh:(61.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint2"].rotation_quaternion[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (41.0, 0.6) lh:(40.7, 0.6 AUTO_CLAMPED) rh:(41.3, 0.6 AUTO_CLAMPED)
    - (42.0, 1.0) lh:(41.7, 0.9 AUTO_CLAMPED) rh:(42.3, 1.0 AUTO_CLAMPED)
    - (43.0, 1.0) lh:(42.7, 1.0 AUTO_CLAMPED) rh:(43.3, 1.0 AUTO_CLAMPED)
      ...
    - (59.0, 1.0) lh:(58.7, 1.0 AUTO_CLAMPED) rh:(59.3, 1.0 AUTO_CLAMPED)
    - (60.0, 1.0) lh:(59.7, 1.0 AUTO_CLAMPED) rh:(60.3, 1.0 AUTO_CLAMPED)
    - (61.0, 1.0) lh:(60.7, 1.0 AUTO_CLAMPED) rh:(61.3, 1.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint2"].rotation_quaternion[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (41.0, 0.8) lh:(40.7, 0.8 AUTO_CLAMPED) rh:(41.3, 0.8 AUTO_CLAMPED)
    - (42.0, 0.3) lh:(41.7, 0.5 AUTO_CLAMPED) rh:(42.3, 0.1 AUTO_CLAMPED)
    - (43.0, 0.0) lh:(42.7, 0.0 AUTO_CLAMPED) rh:(43.3, 0.0 AUTO_CLAMPED)
      ...
    - (59.0, 0.0) lh:(58.7, 0.0 AUTO_CLAMPED) rh:(59.3, 0.0 AUTO_CLAMPED)
    - (60.0, 0.0) lh:(59.7, 0.0 AUTO_CLAMPED) rh:(60.3, 0.0 AUTO_CLAMPED)
    - (61.0, 0.0) lh:(60.7, 0.0 AUTO_CLAMPED) rh:(61.3, 0.0 AUTO_CLAMPED)

- Action 'Armature|spin|BaseLayer' curverange:(21.0 .. 41.0) curves:30
  - fcu 'pose.bones["joint1"].location[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
    - (22.0, 0.0) lh:(21.7, 0.0 AUTO_CLAMPED) rh:(22.3, 0.0 AUTO_CLAMPED)
    - (23.0, 0.0) lh:(22.7, 0.0 AUTO_CLAMPED) rh:(23.3, 0.0 AUTO_CLAMPED)
      ...
    - (39.0, 0.0) lh:(38.7, 0.0 AUTO_CLAMPED) rh:(39.3, 0.0 AUTO_CLAMPED)
    - (40.0, 0.0) lh:(39.7, 0.0 AUTO_CLAMPED) rh:(40.3, 0.0 AUTO_CLAMPED)
    - (41.0, 0.0) lh:(40.7, 0.0 AUTO_CLAMPED) rh:(41.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].location[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
    - (22.0, 0.0) lh:(21.7, 0.0 AUTO_CLAMPED) rh:(22.3, 0.0 AUTO_CLAMPED)
    - (23.0, 0.0) lh:(22.7, 0.0 AUTO_CLAMPED) rh:(23.3, 0.0 AUTO_CLAMPED)
      ...
    - (39.0, 0.0) lh:(38.7, 0.0 AUTO_CLAMPED) rh:(39.3, 0.0 AUTO_CLAMPED)
    - (40.0, 0.0) lh:(39.7, 0.0 AUTO_CLAMPED) rh:(40.3, 0.0 AUTO_CLAMPED)
    - (41.0, 0.0) lh:(40.7, 0.0 AUTO_CLAMPED) rh:(41.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].location[2]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
    - (22.0, 0.0) lh:(21.7, 0.0 AUTO_CLAMPED) rh:(22.3, 0.0 AUTO_CLAMPED)
    - (23.0, 0.0) lh:(22.7, 0.0 AUTO_CLAMPED) rh:(23.3, 0.0 AUTO_CLAMPED)
      ...
    - (39.0, 0.0) lh:(38.7, 0.0 AUTO_CLAMPED) rh:(39.3, 0.0 AUTO_CLAMPED)
    - (40.0, 0.0) lh:(39.7, 0.0 AUTO_CLAMPED) rh:(40.3, 0.0 AUTO_CLAMPED)
    - (41.0, 0.0) lh:(40.7, 0.0 AUTO_CLAMPED) rh:(41.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].rotation_quaternion[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (21.0, 1.0) lh:(20.7, 1.0 AUTO_CLAMPED) rh:(21.3, 1.0 AUTO_CLAMPED)
    - (22.0, 1.0) lh:(21.7, 1.0 AUTO_CLAMPED) rh:(22.3, 1.0 AUTO_CLAMPED)
    - (23.0, 1.0) lh:(22.7, 1.0 AUTO_CLAMPED) rh:(23.3, 1.0 AUTO_CLAMPED)
      ...
    - (39.0, 0.2) lh:(38.7, 0.2 AUTO_CLAMPED) rh:(39.3, 0.2 AUTO_CLAMPED)
    - (40.0, 0.2) lh:(39.7, 0.2 AUTO_CLAMPED) rh:(40.3, 0.2 AUTO_CLAMPED)
    - (41.0, 0.5) lh:(40.7, 0.5 AUTO_CLAMPED) rh:(41.3, 0.5 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].rotation_quaternion[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
    - (22.0, 0.0) lh:(21.7, 0.0 AUTO_CLAMPED) rh:(22.3, 0.0 AUTO_CLAMPED)
    - (23.0, 0.1) lh:(22.7, 0.1 AUTO_CLAMPED) rh:(23.3, 0.1 AUTO_CLAMPED)
      ...
    - (39.0, 1.0) lh:(38.7, 1.0 AUTO_CLAMPED) rh:(39.3, 1.0 AUTO_CLAMPED)
    - (40.0, 1.0) lh:(39.7, 1.0 AUTO_CLAMPED) rh:(40.3, 1.0 AUTO_CLAMPED)
    - (41.0, 0.9) lh:(40.7, 0.9 AUTO_CLAMPED) rh:(41.3, 0.9 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].rotation_quaternion[2]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
    - (22.0, 0.0) lh:(21.7, 0.0 AUTO_CLAMPED) rh:(22.3, 0.0 AUTO_CLAMPED)
    - (23.0, 0.0) lh:(22.7, 0.0 AUTO_CLAMPED) rh:(23.3, 0.0 AUTO_CLAMPED)
      ...
    - (39.0, 0.0) lh:(38.7, 0.0 AUTO_CLAMPED) rh:(39.3, 0.0 AUTO_CLAMPED)
    - (40.0, 0.0) lh:(39.7, 0.0 AUTO_CLAMPED) rh:(40.3, 0.0 AUTO_CLAMPED)
    - (41.0, 0.0) lh:(40.7, 0.0 AUTO_CLAMPED) rh:(41.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].rotation_quaternion[3]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
    - (22.0, 0.0) lh:(21.7, 0.0 AUTO_CLAMPED) rh:(22.3, 0.0 AUTO_CLAMPED)
    - (23.0, 0.0) lh:(22.7, 0.0 AUTO_CLAMPED) rh:(23.3, 0.0 AUTO_CLAMPED)
      ...
    - (39.0, 0.0) lh:(38.7, 0.0 AUTO_CLAMPED) rh:(39.3, 0.0 AUTO_CLAMPED)
    - (40.0, 0.0) lh:(39.7, 0.0 AUTO_CLAMPED) rh:(40.3, 0.0 AUTO_CLAMPED)
    - (41.0, 0.0) lh:(40.7, 0.0 AUTO_CLAMPED) rh:(41.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].scale[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (21.0, 1.0) lh:(20.7, 1.0 AUTO_CLAMPED) rh:(21.3, 1.0 AUTO_CLAMPED)
    - (22.0, 1.0) lh:(21.7, 1.0 AUTO_CLAMPED) rh:(22.3, 1.0 AUTO_CLAMPED)
    - (23.0, 1.0) lh:(22.7, 1.0 AUTO_CLAMPED) rh:(23.3, 1.0 AUTO_CLAMPED)
      ...
    - (39.0, 1.0) lh:(38.7, 1.0 AUTO_CLAMPED) rh:(39.3, 1.0 AUTO_CLAMPED)
    - (40.0, 1.0) lh:(39.7, 1.0 AUTO_CLAMPED) rh:(40.3, 1.0 AUTO_CLAMPED)
    - (41.0, 1.0) lh:(40.7, 1.0 AUTO_CLAMPED) rh:(41.3, 1.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].scale[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (21.0, 1.0) lh:(20.7, 1.0 AUTO_CLAMPED) rh:(21.3, 1.0 AUTO_CLAMPED)
    - (22.0, 1.0) lh:(21.7, 1.0 AUTO_CLAMPED) rh:(22.3, 1.0 AUTO_CLAMPED)
    - (23.0, 1.0) lh:(22.7, 1.0 AUTO_CLAMPED) rh:(23.3, 1.0 AUTO_CLAMPED)
      ...
    - (39.0, 1.0) lh:(38.7, 1.0 AUTO_CLAMPED) rh:(39.3, 1.0 AUTO_CLAMPED)
    - (40.0, 1.0) lh:(39.7, 1.0 AUTO_CLAMPED) rh:(40.3, 1.0 AUTO_CLAMPED)
    - (41.0, 1.0) lh:(40.7, 1.0 AUTO_CLAMPED) rh:(41.3, 1.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].scale[2]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (21.0, 1.0) lh:(20.7, 1.0 AUTO_CLAMPED) rh:(21.3, 1.0 AUTO_CLAMPED)
    - (22.0, 1.0) lh:(21.7, 1.0 AUTO_CLAMPED) rh:(22.3, 1.0 AUTO_CLAMPED)
    - (23.0, 1.0) lh:(22.7, 1.0 AUTO_CLAMPED) rh:(23.3, 1.0 AUTO_CLAMPED)
      ...
    - (39.0, 1.0) lh:(38.7, 1.0 AUTO_CLAMPED) rh:(39.3, 1.0 AUTO_CLAMPED)
    - (40.0, 1.0) lh:(39.7, 1.0 AUTO_CLAMPED) rh:(40.3, 1.0 AUTO_CLAMPED)
    - (41.0, 1.0) lh:(40.7, 1.0 AUTO_CLAMPED) rh:(41.3, 1.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint2"].location[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
    - (22.0, 0.0) lh:(21.7, 0.0 AUTO_CLAMPED) rh:(22.3, 0.0 AUTO_CLAMPED)
    - (23.0, 0.0) lh:(22.7, 0.0 AUTO_CLAMPED) rh:(23.3, 0.0 AUTO_CLAMPED)
      ...
    - (39.0, 0.0) lh:(38.7, 0.0 AUTO_CLAMPED) rh:(39.3, 0.0 AUTO_CLAMPED)
    - (40.0, 0.0) lh:(39.7, 0.0 AUTO_CLAMPED) rh:(40.3, 0.0 AUTO_CLAMPED)
    - (41.0, 0.0) lh:(40.7, 0.0 AUTO_CLAMPED) rh:(41.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint2"].location[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (21.0, -0.0) lh:(20.7, -0.0 AUTO_CLAMPED) rh:(21.3, -0.0 AUTO_CLAMPED)
    - (22.0, -0.0) lh:(21.7, -0.0 AUTO_CLAMPED) rh:(22.3, -0.0 AUTO_CLAMPED)
    - (23.0, -0.0) lh:(22.7, -0.0 AUTO_CLAMPED) rh:(23.3, -0.0 AUTO_CLAMPED)
      ...
    - (39.0, -0.0) lh:(38.7, -0.0 AUTO_CLAMPED) rh:(39.3, -0.0 AUTO_CLAMPED)
    - (40.0, -0.0) lh:(39.7, -0.0 AUTO_CLAMPED) rh:(40.3, -0.0 AUTO_CLAMPED)
    - (41.0, -0.0) lh:(40.7, -0.0 AUTO_CLAMPED) rh:(41.3, -0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint2"].location[2]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
    - (22.0, 0.0) lh:(21.7, 0.0 AUTO_CLAMPED) rh:(22.3, 0.0 AUTO_CLAMPED)
    - (23.0, 0.0) lh:(22.7, 0.0 AUTO_CLAMPED) rh:(23.3, 0.0 AUTO_CLAMPED)
      ...
    - (39.0, 0.0) lh:(38.7, 0.0 AUTO_CLAMPED) rh:(39.3, 0.0 AUTO_CLAMPED)
    - (40.0, 0.0) lh:(39.7, 0.0 AUTO_CLAMPED) rh:(40.3, 0.0 AUTO_CLAMPED)
    - (41.0, 0.0) lh:(40.7, 0.0 AUTO_CLAMPED) rh:(41.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint2"].rotation_quaternion[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (21.0, 1.0) lh:(20.7, 1.0 AUTO_CLAMPED) rh:(21.3, 1.0 AUTO_CLAMPED)
    - (22.0, 1.0) lh:(21.7, 1.0 AUTO_CLAMPED) rh:(22.3, 1.0 AUTO_CLAMPED)
    - (23.0, 1.0) lh:(22.7, 1.0 AUTO_CLAMPED) rh:(23.3, 1.0 AUTO_CLAMPED)
      ...
    - (39.0, 0.4) lh:(38.7, 0.4 AUTO_CLAMPED) rh:(39.3, 0.4 AUTO_CLAMPED)
    - (40.0, 0.4) lh:(39.7, 0.4 AUTO_CLAMPED) rh:(40.3, 0.4 AUTO_CLAMPED)
    - (41.0, 0.6) lh:(40.7, 0.6 AUTO_CLAMPED) rh:(41.3, 0.6 AUTO_CLAMPED)
  - fcu 'pose.bones["joint2"].rotation_quaternion[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:21
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
    - (22.0, 0.0) lh:(21.7, 0.0 AUTO_CLAMPED) rh:(22.3, 0.0 AUTO_CLAMPED)
    - (23.0, 0.1) lh:(22.7, 0.1 AUTO_CLAMPED) rh:(23.3, 0.1 AUTO_CLAMPED)
      ...
    - (39.0, 0.9) lh:(38.7, 0.9 AUTO_CLAMPED) rh:(39.3, 0.9 AUTO_CLAMPED)
    - (40.0, 0.9) lh:(39.7, 0.9 AUTO_CLAMPED) rh:(40.3, 0.9 AUTO_CLAMPED)
    - (41.0, 0.8) lh:(40.7, 0.8 AUTO_CLAMPED) rh:(41.3, 0.8 AUTO_CLAMPED)

- Action 'Armature|wiggle|BaseLayer' curverange:(2.0 .. 21.0) curves:30
  - fcu 'pose.bones["joint1"].location[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:20
    - (2.0, 0.0) lh:(1.7, 0.0 AUTO_CLAMPED) rh:(2.3, 0.0 AUTO_CLAMPED)
    - (3.0, 0.0) lh:(2.7, 0.0 AUTO_CLAMPED) rh:(3.3, 0.0 AUTO_CLAMPED)
    - (4.0, 0.0) lh:(3.7, 0.0 AUTO_CLAMPED) rh:(4.3, 0.0 AUTO_CLAMPED)
      ...
    - (19.0, 0.0) lh:(18.7, 0.0 AUTO_CLAMPED) rh:(19.3, 0.0 AUTO_CLAMPED)
    - (20.0, 0.0) lh:(19.7, 0.0 AUTO_CLAMPED) rh:(20.3, 0.0 AUTO_CLAMPED)
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].location[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:20
    - (2.0, 0.0) lh:(1.7, 0.0 AUTO_CLAMPED) rh:(2.3, 0.0 AUTO_CLAMPED)
    - (3.0, 0.0) lh:(2.7, 0.0 AUTO_CLAMPED) rh:(3.3, 0.0 AUTO_CLAMPED)
    - (4.0, 0.0) lh:(3.7, 0.0 AUTO_CLAMPED) rh:(4.3, 0.0 AUTO_CLAMPED)
      ...
    - (19.0, 0.0) lh:(18.7, 0.0 AUTO_CLAMPED) rh:(19.3, 0.0 AUTO_CLAMPED)
    - (20.0, 0.0) lh:(19.7, 0.0 AUTO_CLAMPED) rh:(20.3, 0.0 AUTO_CLAMPED)
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].location[2]' smooth:CONT_ACCEL extra:CONSTANT keyframes:20
    - (2.0, 0.0) lh:(1.7, 0.0 AUTO_CLAMPED) rh:(2.3, 0.0 AUTO_CLAMPED)
    - (3.0, 0.0) lh:(2.7, 0.0 AUTO_CLAMPED) rh:(3.3, 0.0 AUTO_CLAMPED)
    - (4.0, 0.0) lh:(3.7, 0.0 AUTO_CLAMPED) rh:(4.3, 0.0 AUTO_CLAMPED)
      ...
    - (19.0, 0.0) lh:(18.7, 0.0 AUTO_CLAMPED) rh:(19.3, 0.0 AUTO_CLAMPED)
    - (20.0, 0.0) lh:(19.7, 0.0 AUTO_CLAMPED) rh:(20.3, 0.0 AUTO_CLAMPED)
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].rotation_quaternion[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:20
    - (2.0, 0.9) lh:(1.7, 0.9 AUTO_CLAMPED) rh:(2.3, 0.9 AUTO_CLAMPED)
    - (3.0, 0.9) lh:(2.7, 0.9 AUTO_CLAMPED) rh:(3.3, 0.9 AUTO_CLAMPED)
    - (4.0, 0.9) lh:(3.7, 0.9 AUTO_CLAMPED) rh:(4.3, 1.0 AUTO_CLAMPED)
      ...
    - (19.0, 1.0) lh:(18.7, 1.0 AUTO_CLAMPED) rh:(19.3, 1.0 AUTO_CLAMPED)
    - (20.0, 1.0) lh:(19.7, 1.0 AUTO_CLAMPED) rh:(20.3, 1.0 AUTO_CLAMPED)
    - (21.0, 1.0) lh:(20.7, 1.0 AUTO_CLAMPED) rh:(21.3, 1.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].rotation_quaternion[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:20
    - (2.0, 0.0) lh:(1.7, 0.0 AUTO_CLAMPED) rh:(2.3, 0.0 AUTO_CLAMPED)
    - (3.0, 0.0) lh:(2.7, 0.0 AUTO_CLAMPED) rh:(3.3, 0.0 AUTO_CLAMPED)
    - (4.0, 0.0) lh:(3.7, 0.0 AUTO_CLAMPED) rh:(4.3, 0.0 AUTO_CLAMPED)
      ...
    - (19.0, 0.0) lh:(18.7, 0.0 AUTO_CLAMPED) rh:(19.3, 0.0 AUTO_CLAMPED)
    - (20.0, 0.0) lh:(19.7, 0.0 AUTO_CLAMPED) rh:(20.3, 0.0 AUTO_CLAMPED)
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].rotation_quaternion[2]' smooth:CONT_ACCEL extra:CONSTANT keyframes:20
    - (2.0, 0.0) lh:(1.7, 0.0 AUTO_CLAMPED) rh:(2.3, 0.0 AUTO_CLAMPED)
    - (3.0, 0.0) lh:(2.7, 0.0 AUTO_CLAMPED) rh:(3.3, 0.0 AUTO_CLAMPED)
    - (4.0, 0.0) lh:(3.7, 0.0 AUTO_CLAMPED) rh:(4.3, 0.0 AUTO_CLAMPED)
      ...
    - (19.0, 0.0) lh:(18.7, 0.0 AUTO_CLAMPED) rh:(19.3, 0.0 AUTO_CLAMPED)
    - (20.0, 0.0) lh:(19.7, 0.0 AUTO_CLAMPED) rh:(20.3, 0.0 AUTO_CLAMPED)
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].rotation_quaternion[3]' smooth:CONT_ACCEL extra:CONSTANT keyframes:20
    - (2.0, -0.4) lh:(1.7, -0.4 AUTO_CLAMPED) rh:(2.3, -0.4 AUTO_CLAMPED)
    - (3.0, -0.4) lh:(2.7, -0.4 AUTO_CLAMPED) rh:(3.3, -0.4 AUTO_CLAMPED)
    - (4.0, -0.3) lh:(3.7, -0.3 AUTO_CLAMPED) rh:(4.3, -0.3 AUTO_CLAMPED)
      ...
    - (19.0, -0.3) lh:(18.7, -0.3 AUTO_CLAMPED) rh:(19.3, -0.3 AUTO_CLAMPED)
    - (20.0, -0.1) lh:(19.7, -0.2 AUTO_CLAMPED) rh:(20.3, -0.1 AUTO_CLAMPED)
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].scale[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:20
    - (2.0, 1.0) lh:(1.7, 1.0 AUTO_CLAMPED) rh:(2.3, 1.0 AUTO_CLAMPED)
    - (3.0, 1.0) lh:(2.7, 1.0 AUTO_CLAMPED) rh:(3.3, 1.0 AUTO_CLAMPED)
    - (4.0, 1.0) lh:(3.7, 1.0 AUTO_CLAMPED) rh:(4.3, 1.0 AUTO_CLAMPED)
      ...
    - (19.0, 1.0) lh:(18.7, 1.0 AUTO_CLAMPED) rh:(19.3, 1.0 AUTO_CLAMPED)
    - (20.0, 1.0) lh:(19.7, 1.0 AUTO_CLAMPED) rh:(20.3, 1.0 AUTO_CLAMPED)
    - (21.0, 1.0) lh:(20.7, 1.0 AUTO_CLAMPED) rh:(21.3, 1.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].scale[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:20
    - (2.0, 1.0) lh:(1.7, 1.0 AUTO_CLAMPED) rh:(2.3, 1.0 AUTO_CLAMPED)
    - (3.0, 1.0) lh:(2.7, 1.0 AUTO_CLAMPED) rh:(3.3, 1.0 AUTO_CLAMPED)
    - (4.0, 1.0) lh:(3.7, 1.0 AUTO_CLAMPED) rh:(4.3, 1.0 AUTO_CLAMPED)
      ...
    - (19.0, 1.0) lh:(18.7, 1.0 AUTO_CLAMPED) rh:(19.3, 1.0 AUTO_CLAMPED)
    - (20.0, 1.0) lh:(19.7, 1.0 AUTO_CLAMPED) rh:(20.3, 1.0 AUTO_CLAMPED)
    - (21.0, 1.0) lh:(20.7, 1.0 AUTO_CLAMPED) rh:(21.3, 1.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint1"].scale[2]' smooth:CONT_ACCEL extra:CONSTANT keyframes:20
    - (2.0, 1.0) lh:(1.7, 1.0 AUTO_CLAMPED) rh:(2.3, 1.0 AUTO_CLAMPED)
    - (3.0, 1.0) lh:(2.7, 1.0 AUTO_CLAMPED) rh:(3.3, 1.0 AUTO_CLAMPED)
    - (4.0, 1.0) lh:(3.7, 1.0 AUTO_CLAMPED) rh:(4.3, 1.0 AUTO_CLAMPED)
      ...
    - (19.0, 1.0) lh:(18.7, 1.0 AUTO_CLAMPED) rh:(19.3, 1.0 AUTO_CLAMPED)
    - (20.0, 1.0) lh:(19.7, 1.0 AUTO_CLAMPED) rh:(20.3, 1.0 AUTO_CLAMPED)
    - (21.0, 1.0) lh:(20.7, 1.0 AUTO_CLAMPED) rh:(21.3, 1.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint2"].location[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:20
    - (2.0, 0.0) lh:(1.7, 0.0 AUTO_CLAMPED) rh:(2.3, 0.0 AUTO_CLAMPED)
    - (3.0, 0.0) lh:(2.7, 0.0 AUTO_CLAMPED) rh:(3.3, 0.0 AUTO_CLAMPED)
    - (4.0, 0.0) lh:(3.7, 0.0 AUTO_CLAMPED) rh:(4.3, 0.0 AUTO_CLAMPED)
      ...
    - (19.0, 0.0) lh:(18.7, 0.0 AUTO_CLAMPED) rh:(19.3, 0.0 AUTO_CLAMPED)
    - (20.0, 0.0) lh:(19.7, 0.0 AUTO_CLAMPED) rh:(20.3, 0.0 AUTO_CLAMPED)
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint2"].location[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:20
    - (2.0, -0.0) lh:(1.7, -0.0 AUTO_CLAMPED) rh:(2.3, -0.0 AUTO_CLAMPED)
    - (3.0, -0.0) lh:(2.7, -0.0 AUTO_CLAMPED) rh:(3.3, -0.0 AUTO_CLAMPED)
    - (4.0, -0.0) lh:(3.7, -0.0 AUTO_CLAMPED) rh:(4.3, -0.0 AUTO_CLAMPED)
      ...
    - (19.0, -0.0) lh:(18.7, -0.0 AUTO_CLAMPED) rh:(19.3, -0.0 AUTO_CLAMPED)
    - (20.0, -0.0) lh:(19.7, -0.0 AUTO_CLAMPED) rh:(20.3, -0.0 AUTO_CLAMPED)
    - (21.0, -0.0) lh:(20.7, -0.0 AUTO_CLAMPED) rh:(21.3, -0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint2"].location[2]' smooth:CONT_ACCEL extra:CONSTANT keyframes:20
    - (2.0, 0.0) lh:(1.7, 0.0 AUTO_CLAMPED) rh:(2.3, 0.0 AUTO_CLAMPED)
    - (3.0, 0.0) lh:(2.7, 0.0 AUTO_CLAMPED) rh:(3.3, 0.0 AUTO_CLAMPED)
    - (4.0, 0.0) lh:(3.7, 0.0 AUTO_CLAMPED) rh:(4.3, 0.0 AUTO_CLAMPED)
      ...
    - (19.0, 0.0) lh:(18.7, 0.0 AUTO_CLAMPED) rh:(19.3, 0.0 AUTO_CLAMPED)
    - (20.0, 0.0) lh:(19.7, 0.0 AUTO_CLAMPED) rh:(20.3, 0.0 AUTO_CLAMPED)
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint2"].rotation_quaternion[0]' smooth:CONT_ACCEL extra:CONSTANT keyframes:20
    - (2.0, 0.8) lh:(1.7, 0.8 AUTO_CLAMPED) rh:(2.3, 0.8 AUTO_CLAMPED)
    - (3.0, 0.9) lh:(2.7, 0.8 AUTO_CLAMPED) rh:(3.3, 0.9 AUTO_CLAMPED)
    - (4.0, 0.9) lh:(3.7, 0.9 AUTO_CLAMPED) rh:(4.3, 0.9 AUTO_CLAMPED)
      ...
    - (19.0, 0.9) lh:(18.7, 0.9 AUTO_CLAMPED) rh:(19.3, 0.9 AUTO_CLAMPED)
    - (20.0, 1.0) lh:(19.7, 1.0 AUTO_CLAMPED) rh:(20.3, 1.0 AUTO_CLAMPED)
    - (21.0, 1.0) lh:(20.7, 1.0 AUTO_CLAMPED) rh:(21.3, 1.0 AUTO_CLAMPED)
  - fcu 'pose.bones["joint2"].rotation_quaternion[1]' smooth:CONT_ACCEL extra:CONSTANT keyframes:20
    - (2.0, 0.0) lh:(1.7, 0.0 AUTO_CLAMPED) rh:(2.3, 0.0 AUTO_CLAMPED)
    - (3.0, 0.0) lh:(2.7, 0.0 AUTO_CLAMPED) rh:(3.3, 0.0 AUTO_CLAMPED)
    - (4.0, 0.0) lh:(3.7, 0.0 AUTO_CLAMPED) rh:(4.3, 0.0 AUTO_CLAMPED)
      ...
    - (19.0, 0.0) lh:(18.7, 0.0 AUTO_CLAMPED) rh:(19.3, 0.0 AUTO_CLAMPED)
    - (20.0, 0.0) lh:(19.7, 0.0 AUTO_CLAMPED) rh:(20.3, 0.0 AUTO_CLAMPED)
    - (21.0, 0.0) lh:(20.7, 0.0 AUTO_CLAMPED) rh:(21.3, 0.0 AUTO_CLAMPED)

==== Armatures: 1
- Armature 'Armature' 4 bones
  - bone 'joint1' h:(0.000, 0.000, 0.000) t:(-1.000, 0.000, 0.000) radius h:0.100 t:0.050
      0.000 -1.000 0.000 0.000
      1.000 0.000 0.000 0.000
      0.000 0.000 1.000 0.000
  - bone 'joint2' parent:'joint1' h:(1.000, -1.000, 0.000) t:(1.000, 0.154, 0.000) radius h:0.100 t:0.050
      0.000 -1.000 0.000 0.000
      1.000 0.000 0.000 1.000
      0.000 0.000 1.000 0.000
  - bone 'joint3' parent:'joint2' h:(1.154, -1.154, 0.000) t:(1.154, -0.081, 0.000) radius h:0.100 t:0.050
      0.000 -1.000 0.000 0.000
      1.000 0.000 0.000 2.154
      0.000 0.000 1.000 0.000
  - bone 'joint4' parent:'joint3' h:(1.074, -1.074, 0.000) t:(2.147, -1.074, 0.000) radius h:0.100 t:0.050
      1.000 0.000 0.000 0.000
      0.000 1.000 0.000 3.228
      0.000 0.000 1.000 0.000

